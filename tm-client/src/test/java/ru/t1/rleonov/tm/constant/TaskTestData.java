package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.model.TaskDTO;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class TaskTestData {

    @NotNull
    public final static TaskDTO TASK1 = new TaskDTO("Task1", "Task1");

    @NotNull
    public final static TaskDTO TASK2 = new TaskDTO("Task2", "Task2");

    @NotNull
    public final static TaskDTO TASK3 = new TaskDTO("Task3", "Task3");

    @NotNull
    public final static TaskDTO NEW_TASK = new TaskDTO("NEW_TASK", "NEW_TASK");

    @NotNull
    public final static List<TaskDTO> USER1_TASKS = Arrays.asList(TASK1, TASK2);

    @NotNull
    public final static List<TaskDTO> USER2_TASKS = Collections.singletonList(TASK3);

}
