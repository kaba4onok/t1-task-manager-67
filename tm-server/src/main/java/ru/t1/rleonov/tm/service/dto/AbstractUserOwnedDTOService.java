package ru.t1.rleonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.rleonov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.rleonov.tm.repository.dto.AbstractUserOwnedDTORepository;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

    @NotNull
    @Autowired
    protected AbstractUserOwnedDTORepository<M> repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return;
        repository.saveAll(models);
    }

}
