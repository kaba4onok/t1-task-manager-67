package ru.t1.rleonov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.Task;
import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

}
