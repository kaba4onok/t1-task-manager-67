package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.model.IUserOwnedService;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import ru.t1.rleonov.tm.repository.model.AbstractUserOwnedRepository;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

    @NotNull
    @Autowired
    protected AbstractUserOwnedRepository<M> repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return;
        repository.saveAll(models);
    }

}
