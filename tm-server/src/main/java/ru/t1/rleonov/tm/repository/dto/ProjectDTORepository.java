package ru.t1.rleonov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> {

    void deleteByUserId(@NotNull String userId);

    @Nullable
    ProjectDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

}