package ru.t1.rleonov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum UserSort {

    BY_NAME("Sort by name"),
    BY_STATUS("Sort by status"),
    BY_CREATED("Sort by created");

    @NotNull
    private final String displayName;

    UserSort(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static UserSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final UserSort userSort : values()) {
            if (userSort.name().equals(value)) return userSort;
        }
        return null;
    }

}
