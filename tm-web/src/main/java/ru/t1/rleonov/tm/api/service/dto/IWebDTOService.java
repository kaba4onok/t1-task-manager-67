package ru.t1.rleonov.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.model.dto.AbstractModelDTO;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.enumerated.UserSort;
import java.util.Collection;
import java.util.List;

public interface IWebDTOService<M extends AbstractModelDTO> {

    void clear(@Nullable String userId);

    void clear();

    @SneakyThrows
    @Transactional
    void clear(@NotNull List<M> models);

    void set(@NotNull Collection<M> models);

    @NotNull
    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    M removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    List<M> findAll(@Nullable String userId,
                    @Nullable UserSort userSort
    );

    @Nullable
    M findOneById(@Nullable String id);

    void removeById(@Nullable String id);

    void remove(@Nullable M model);

    @Nullable
    M save(@Nullable M model);

    @SneakyThrows
    long count();

    @NotNull
    List<M> findAll();

}
