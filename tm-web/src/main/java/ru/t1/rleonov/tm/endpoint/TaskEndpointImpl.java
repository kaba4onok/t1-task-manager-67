package ru.t1.rleonov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.model.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.rleonov.tm.api.endpoint.ITaskRestEndpoint")
public class TaskEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService taskService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    ) {
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findOneById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findOneById(id) != null;
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.removeById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    ) {
        taskService.remove(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDTO> tasks
    ) {
        taskService.clear(tasks);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}
