package ru.t1.rleonov.tm.api.service.web;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.model.web.TaskWeb;

public interface ITaskService extends IService<TaskWeb> {

    @NotNull
    @SneakyThrows
    @Transactional
    TaskWeb save(@NotNull TaskWeb task);

    @NotNull
    @SneakyThrows
    @Transactional
    void deleteByProjectId(@NotNull String projectId);

}
