package ru.t1.rleonov.tm.service.web;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.t1.rleonov.tm.api.service.web.IService;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractService<M> implements IService<M> {

    @NotNull
    @Autowired
    protected JpaRepository<M, String> repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

}
