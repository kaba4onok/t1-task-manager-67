package ru.t1.rleonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.dto.IWebDTOService;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.model.dto.AbstractModelDTO;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractWebDTOService<M extends AbstractModelDTO> implements IWebDTOService<M> {

    @NotNull
    @Autowired
    protected JpaRepository<M, String> repository;

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public M save(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.save(model);
    }

    @Override
    @SneakyThrows
    public long count() {
        return repository.count();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@NotNull List<M> models) {
        if (models.isEmpty()) throw new EntityNotFoundException();
        repository.deleteAll(models);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return;
        repository.saveAll(models);
    }

}
