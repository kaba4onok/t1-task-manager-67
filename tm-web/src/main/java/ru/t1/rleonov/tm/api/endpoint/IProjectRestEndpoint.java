package ru.t1.rleonov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @WebMethod
    @PostMapping("/save")
    ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    );

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody List<ProjectDTO> projects
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();

}
