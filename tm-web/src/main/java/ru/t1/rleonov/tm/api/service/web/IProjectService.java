package ru.t1.rleonov.tm.api.service.web;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.web.ProjectWeb;

public interface IProjectService extends IService<ProjectWeb> {

    @NotNull
    ProjectWeb save(@NotNull ProjectWeb project);

}
