package ru.t1.rleonov.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.rleonov.tm")
public class ApplicationConfiguration {

}
