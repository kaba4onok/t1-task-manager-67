package ru.t1.rleonov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.rleonov.tm.api.endpoint.IProjectRestEndpoint")
public class ProjectEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    ) {
        return projectService.save(project);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return projectService.findOneById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return projectService.findOneById(id) != null;
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDTO project
    ) {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody List<ProjectDTO> projects
    ) {
        projectService.clear(projects);
    }

    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}
