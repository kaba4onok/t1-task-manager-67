package ru.t1.rleonov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.model.dto.TaskDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @WebMethod
    @PostMapping("/save")
    TaskDTO save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    );

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDTO task
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDTO> tasks
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();

}
