package ru.t1.rleonov.tm.model.web;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.rleonov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskWeb {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @Column(nullable = true, name = "user_id")
    private String userId;

    @Column
    @Nullable
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskWeb(@NotNull final String name) {
        this.name = name;
    }

}
