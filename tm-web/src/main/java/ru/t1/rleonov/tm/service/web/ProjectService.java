package ru.t1.rleonov.tm.service.web;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.web.IProjectService;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.exception.field.NameEmptyException;
import ru.t1.rleonov.tm.model.web.ProjectWeb;
import ru.t1.rleonov.tm.repository.web.ProjectWebRepository;

@Service
@NoArgsConstructor
public class ProjectService extends AbstractService<ProjectWeb>
        implements IProjectService {

    @NotNull
    @Autowired
    public ProjectWebRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectWeb save(@NotNull final ProjectWeb project) {
        if (project.getId().isEmpty()) throw new IdEmptyException();
        if (project.getName() == null || project.getName().isEmpty()) throw new NameEmptyException();
        return repository.save(project);
    }

}
