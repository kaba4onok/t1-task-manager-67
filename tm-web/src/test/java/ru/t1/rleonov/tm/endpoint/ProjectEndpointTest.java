package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.client.ProjectEndpointClient;
import ru.t1.rleonov.tm.marker.IntegrationCategory;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import java.util.List;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;

public class ProjectEndpointTest {

    @NotNull
    static final ProjectEndpointClient client = ProjectEndpointClient.client();

    @BeforeClass
    public static void setUpClass() {
        client.clear();
    }

    @Before
    public void setUp() {
        client.save(PROJECT1);
        client.save(PROJECT2);
        client.save(PROJECT3);
    }

    @After
    public void reset() {
        client.clear();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @NotNull final List<ProjectDTO> projects = client.findAll();
        Assert.assertEquals(PROJECTS.size(), projects.size());
        Assert.assertEquals(PROJECTS.get(0).getId(), projects.get(0).getId());
        Assert.assertEquals(PROJECTS.get(1).getId(), projects.get(1).getId());
        Assert.assertEquals(PROJECTS.get(2).getId(), projects.get(2).getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final ProjectDTO project = client.save(PROJECT_NEW);
        Assert.assertEquals(project.getId(), PROJECT_NEW.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String id = PROJECT1.getId();
        @NotNull final ProjectDTO project = client.findById(id);
        Assert.assertEquals(project.getId(), id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsById() {
        Assert.assertTrue(client.existsById(PROJECT1.getId()));
        Assert.assertFalse(client.existsById(PROJECT_NEW.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void count() {
        Assert.assertEquals(client.count(), PROJECTS.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        @NotNull final String id = PROJECT1.getId();
        Assert.assertNotNull(client.findById(id));
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void delete() {
        @NotNull final String id = PROJECT1.getId();
        Assert.assertNotNull(client.findById(id));
        client.delete(PROJECT1);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteAll() {
        Assert.assertEquals(client.findAll().size(), PROJECTS.size());
        client.save(PROJECT_NEW);
        client.deleteAll(PROJECTS);
        Assert.assertEquals(client.findAll().size(), 1);
        Assert.assertEquals(client.findAll().get(0).getId(), PROJECT_NEW.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clear() {
        Assert.assertEquals(client.findAll().size(), PROJECTS.size());
        client.clear();
        Assert.assertEquals(client.findAll().size(), 0);
    }

}
