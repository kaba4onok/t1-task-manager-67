package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import java.util.Arrays;
import java.util.List;

public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO PROJECT1 = new ProjectDTO("Project1", "Project1");

    @NotNull
    public final static ProjectDTO PROJECT2 = new ProjectDTO("Project2", "Project2");

    @NotNull
    public final static ProjectDTO PROJECT3 = new ProjectDTO("Project3", "Project3");

    @NotNull
    public final static ProjectDTO PROJECT_NEW = new ProjectDTO("Project_New", "Project_New");

    @NotNull
    public final static List<ProjectDTO> PROJECTS = Arrays.asList(PROJECT1, PROJECT2, PROJECT3);

}
